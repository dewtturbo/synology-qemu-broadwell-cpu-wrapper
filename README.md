# Synology qemu Broadwell cpu wrapper

Simple wrapper script for /usr/local/bin/qemu-system-x86_64 that allows you to set the CPU DSM uses to launch VMs. Just move /usr/local/bin/qemu-system-x86_64 to /usr/local/bin/qemu-system-x86_64_real and place the qemu-system-x86_64 wrapper into /usr/local/bin and chmod +x.

When you launch a VM from VMM it will now startup with -cpu Broadwell specified instead of -cpu host. This allows you to use Windows 10/Windows Server 2016,2019 with VMM.

Notes:

DSM will re-create the /usr/local/bin/qemu-system-x86_64 symlink on reboot. You must rm the symlink and re-copy the wrapper everytime you reboot or automate the process.

VMM will show VMs as still running after shutting them down when using this script. You may have to kill the qemu process manually to completely stop the VM.